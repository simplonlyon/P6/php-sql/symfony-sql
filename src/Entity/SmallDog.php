<?php

namespace App\Entity;

class SmallDog {
    public $id;
    public $name;
    public $breed;
    public $age;
    
    /**
     * Méthode qui assigne à l'instance de chien les valeurs contenues
     * dans un tableau associatif d'une ligne de résultat pdo.
     */
    public function fromSQL(array $sql) {
        $this->id = $sql["id"];
        $this->name = $sql["name"];
        $this->breed = $sql["breed"];
        $this->age = $sql["age"];
    }
}