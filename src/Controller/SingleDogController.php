<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Repository\DogRepository;
use App\Form\SmallDogType;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
* @Security("has_role('ROLE_ADMIN')")
*/
class SingleDogController extends Controller
{
    /**
     * @Route("/dog/{id}", name="dog")
     */
    public function index(int $id , DogRepository $repo, Request $request)
    {
        $dog = $repo->getById($id);

        $form = $this->createForm(SmallDogType::class, $dog);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $repo->update($form->getData());
            return $this->redirectToRoute("home");
        }

        return $this->render('single_dog/index.html.twig', [
            "form" => $form->createView(),
            "dog" => $dog
        ]);
    }

    /**
     * @Route("/dog/remove/{id}", name="remove_dog")
     */
    public function remove(int $id, DogRepository $repo) {
        $repo->delete($id);
        return $this->redirectToRoute("home");
    }
}
