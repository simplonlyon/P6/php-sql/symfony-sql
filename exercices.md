# Exercices

## TP Affichien
1. Dans le HomeController, commenter la partie création d'une instance de chien et ajout de chien pour n'avoir au final que le return
2. Toujours dans la route index, utiliser le DogRepository (qui est en argument de la méthode du controller) pour faire un getAll() et stocker le retour du getAll dans une variable
3. Exposer cette variable dans le template avec le render
4. Côté template, faire une boucle for each sur la variable que vous venez d'exposer pour parcourir le tableau de chien, et faire en sorte d'afficher le name de chacun des chien, dans un paragrahpe
5. Dans le template base.html.twig, rajouter le link de la librairie bootstrap directement dans le head (pas dans le block stylesheet), et rajouter en bas du body les javascripts de bootstrap (hors du block javascript)
6. Faire en sorte d'afficher les chiens sous formes de cards bootstrap, comme dans cet exemple là : https://getbootstrap.com/docs/4.1/components/card/#using-grid-markup

## Formulaire d'ajout de chien :
1. Créer un nouveau controller (avec la commande make:controller)  AddDogController(modifié)
2. Dans les argument de la méthode index du controller, rajouter le DogRepository comme dans le HomeController
3. Toujours dans la méthode index du controller, créer un formulaire pour la classe SmallDog et l'exposer dans le render
4. Dans le template, ajouter les balises d'affichages du formulaire
5. Dans la méthode du controller, rajouter un deuxième argument Request après le repository
6. Faire le handlerequest puis dire que si le formulaire est submit et valide, alors on fait un add sur la variable contenant le DogRepository en lui donnant en argument les données du formulaire